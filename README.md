# WTC Memcached

This project is based on the official Memcached image [can be found here](https://hub.docker.com/_/memcached/) (https://hub.docker.com/_/memcached/). Credit goes to the docker team.

The purpose of this image is to make older versions of Memcached available, to support older codebases.

[![](https://badge.imagelayers.io/wondeotec/wtc-memcached:latest.svg)](https://imagelayers.io/?images=wondeotec/wtc-memcached:latest 'Get your own badge on imagelayers.io')

## Configuration and usage

[Please see the official image's README](https://github.com/docker-library/docs/tree/master/memcached).